import axios from 'axios';

axios.defaults.baseURL = process.env.NEXT_PUBLIC_SERVER_BASE_URL

const fetcher = async (url) => {
  try {
    const res = await axios.get(url);
    return res.data;
  } catch (err) {
    throw err.response.data;
  }
};

export default fetcher;
