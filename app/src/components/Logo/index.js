import LocalShippingIcon from '@material-ui/icons/LocalShipping';
import {makeStyles} from '@material-ui/core/styles';
import {Box} from '@material-ui/core';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles((theme) => ({
  icon: {
    margin: theme.spacing(1.5),
  },
}));

function Logo() {
  const classes = useStyles();

  return (
    <Box display="flex" alignItems="center">
      <LocalShippingIcon className={classes.icon} />
      <Typography variant="subtitle2">
        Despáchalo
      </Typography>
    </Box>
  );
}

export default Logo;
