import PropTypes from 'prop-types';
import {ListItem, ListItemText} from '@material-ui/core';
import {makeStyles} from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    justifyContent: 'left',
    margin: theme.spacing(0),
    padding: theme.spacing(0),
  },
  item: {
    display: 'flex',
    justifyContent: 'left',
    margin: theme.spacing(2, 1, 2, 2),
    padding: theme.spacing(0),
    color: theme.palette.common.white,
  },
}));

function OptionItem({id, label, url, handleClick}) {
  const classes = useStyles();

  return (
    <ListItem
      button
      key={id}
      alignItems="center"
      onClick={handleClick}
      className={classes.root}
    >
      <ListItemText
        className={classes.item}
        primary={label}
      />
    </ListItem>
  );
}

OptionItem.propTypes = {
  id: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
  handleClick: PropTypes.func.isRequired,
};

export default OptionItem;
