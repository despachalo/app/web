import {Box} from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import {makeStyles} from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  distCenter: {
    marginLeft: theme.spacing(1),
  },
}));

function DistributionCenter() {
  const classes = useStyles();
  const distributionCenter = 'Lima';

  return (
    <Box display="flex" alignItems="center">
      <Typography variant="subtitle2">
        Centro de Distribución:
      </Typography>
      <Typography
        className={classes.distCenter}
        variant="body2"
      >
        {distributionCenter}
      </Typography>
    </Box>
  );
}

export default DistributionCenter;
