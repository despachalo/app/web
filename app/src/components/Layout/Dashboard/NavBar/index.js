import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import MuiAppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Logo from 'components/Logo';
import DistributionCenter from './DistributionCenter';

const useStyles = makeStyles(() => ({
  root: {
    flexGrow: 1,
  },
  toolbar: {
    display: 'flex',
    flexDirection: 'row-reverse',
    justifyContent: 'space-between',
  },
  logo: {
    flexGrow: 1,
  },
}));

function NavBar() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <MuiAppBar position="static" color="secondary">
        <Toolbar variant="dense" className={classes.toolbar}>
          <Logo />
          <DistributionCenter />
        </Toolbar>
      </MuiAppBar>
    </div>
  );
}

export default NavBar;
