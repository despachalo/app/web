import PropTypes from 'prop-types';
import Title from './Title';

function BlankLayout({children, title}) {
  return (
    <>
      <Title title={title} />
      {children}
    </>
  );
}

BlankLayout.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
};

export default BlankLayout;
