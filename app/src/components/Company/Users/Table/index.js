import {DataGrid} from '@material-ui/data-grid';

const columns = [
  {
    field: 'fullName',
    headerName: 'Nombres',
    width: 250,
    sortable: false,
  },
  {
    field: 'document',
    headerName: 'Documento',
    width: 200,
    sortable: false,
  },
  {
    field: 'role',
    headerName: 'Rol',
    width: 250,
    sortable: false,
  },
  {
    field: 'center',
    headerName: 'Centro de Distribución',
    width: 250,
    sortable: false,
  },
  {
    field: 'state',
    headerName: 'Estado',
    width: 150,
    sortable: false,
  },
];

const rows = [
  {
    id: 1,
    fullName: 'Jhair Guzmán',
    document: 'DNI - 70266899',
    role: 'Analista de Despacho',
    center: 'Lima',
    state: 'ACTIVO',
  },
];

function UsersTable() {
  const height = rows.length * 70 + 120;

  return (
    <div style={{height, width: '100%'}}>
      <DataGrid
        rows={rows}
        columns={columns}
        pageSize={10}
        rowCount={100}
        onPageChange={() => {}}
      />
    </div>
  );
}

export default UsersTable;
