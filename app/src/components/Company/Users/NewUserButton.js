import Button from '@material-ui/core/Button';

function NewUserButton() {
  return (
    <Button color="primary" variant="contained">
      Nuevo Usuario
    </Button>
  );
}

export default NewUserButton;
