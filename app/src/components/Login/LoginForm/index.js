import {withFormik} from 'formik';
import loginSchema from './schema';
import LoginForm from './LoginForm';

const formikHoc = withFormik({
  mapPropsToValues: ({defaultValues: {email, password} = {}}) => ({
    email: email || '',
    password: password || '',
  }),
  validationSchema: loginSchema,

  handleSubmit: (values, {setSubmitting}) => {
    setSubmitting(true);
    console.log('submit!!');
    setTimeout(() => {
      alert(JSON.stringify(values, null, 2));
      setSubmitting(false);
    }, 1000);
  },

  displayName: 'LoginFormFormikHoc',
});

export default formikHoc(LoginForm);
