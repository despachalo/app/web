import * as Yup from 'yup';

const loginSchema = Yup.object().shape({
  email: Yup.string()
    .email('Ingrese un correo válido (ejm: ejemplo@acme.com)')
    .required('Ingrese su correo electrónico'),
  password: Yup.string()
    .required('Ingrese su contraseña'),
});

export default loginSchema;
