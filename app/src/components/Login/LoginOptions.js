import Grid from '@material-ui/core/Grid';
import Link from 'components/Custom/Link';

function LoginOptions() {
  return (
    <Grid container>
      <Grid item xs>
        <Link href="/" variant="body2">
          ¿Olvidaste tu contraseña?
        </Link>
      </Grid>
    </Grid>
  );
}

export default LoginOptions;
