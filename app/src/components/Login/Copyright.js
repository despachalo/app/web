import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';

function Copyright({...props}) {
  return (
    <Typography variant="body2" color="textSecondary" align="center" {...props}>
      {'Copyright © '}
      <Link color="inherit" href="/">
        Despáchalo PE
      </Link>
      {' '}
      {new Date().getFullYear()}
      .
    </Typography>
  );
}

export default Copyright;
